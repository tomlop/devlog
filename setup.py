import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="Carlier-Loppinet", # Replace with your own username
    version="0.0.3",
    author="Carlier-Loppinet",
    author_email="maxime.carlier@insa-lyon.fr",
    description="A small example package realised through Software Deployment module",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/tomlop/devlog",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)

