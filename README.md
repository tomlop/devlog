# DevLog

This project is a bio-inspired optimisation algorithm. It aims at generating a virutal population, evaluate it according to certain conditions and find a set of parameters minimizing the cost function (i.e make the genome as good as possible). 
We will mutate some genomes of the population and modify the value of our parameters in order to converge to an optimal mutation of our genome. 
A virtual population is a set of virtual genomes which is a set of virtual genes. 

Write a clean README.md file :
- describe the project (potentially two audiences) developers and users
- explain what the program solves and detail the installation instructions

INSTALLATION 

pip install -i https://test.pypi.org/simple/ Carlier-Loppinet==0.0.3

You have to install python3 or python4. 

FAQ
- What is a good genome ?
    For this project. We define a good genome as an alternation of -1 and +1 genes. When the same gene appears several times in a row (+1,+1,+1...), the evaluation of the genome will overlap the threshold and the genome will not be considered as a good genome. 

- What is the threshold r ? 
    r represents a maximum authorized step. It constrains the genome to stay in a certain gap. If the genome goes beyond this gap, there will be a bad evaluation of the genome. 

CONTRIBUTE
issue tracker link : 
source codelink : https://gitlab.com/tomlop/devlog.git

TODO
Choose another stopping condition for the determination of the optimized population. For example, instead of a number of iteration, we can implemente decide that the program ends when the mean score of the genomes does not decrease anymore. We can also implemente a limit value for the score value of the best genome (0 or 50)


