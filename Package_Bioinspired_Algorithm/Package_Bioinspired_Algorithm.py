# -*- coding: utf-8 -*-
"""
Created on Tue Nov  5 16:54:05 2019

@author: Carlier, Loppinet

This project is a bio-inspired optimisation algorithm. It aims at generating a virtual population of genomes and find a set of parameters minimizing the cost function. 
We will mutate the genome et modify the value of our parameters in order to converge to an optimal mutation of our genome. 
The genome will be a set of genes and we will have a set of genomes. 
The function cost is our optimisation parameter. 
"""
import random
import numpy as np
import matplotlib.pyplot as plt

def createGenome(taille):
    """Function that creates a genome. The size is given as a parameter and then a list containing 1 or -1 is returned.

    Parameters
    ----------
    taille : int
        The size the user wants to give to the genome.

    Returns
    -------
    list
        A list containing the genome. The size of the list is the size given by the user and each value is chosen randomnly (-1 or 1)


    >>> random.seed(1)
    >>> createGenome(10)
    [-1, -1, 1, -1, 1, 1, 1, 1, -1, -1]
    
    """
    genome=[]
    for i in range(taille):
        if random.randint(0,1)==0:
            genome.append(-1)
        else:
            genome.append(1)
    return genome
    
def evaluate(genome,r):
    """Function that evaluates a genome. A genome and the threshold must be passed as a parameter for the function to calculate the cost. :math:`a^2 + b^2 = c^2`
      
    Parameters
    ----------
    genome : list of ints
          A list representing the genome
          
    r : int
          Threshold
          
    Returns
    -------
    cost : int
          The cost of the genome.


    >>> evaluate([-1, -1, 1, -1, 1, 1, 1, 1, -1, -1],1)
    3
    """
    place=0
    cost=0
    for val in genome:
      place+=val
      if place < (-r) or place>r :
        cost+=1
    return cost

def createPopulation(sizeT, totalNumPop):
    """ Function that generates a particular number of genomes with a particular number of genes. 
    
        Parameters
        ----------
        sizeT : int
            The number of genes for each population's genome
        totalNumPop : int
            The number of genomes in our population
        
        Returns
        -------
        list
            list of genomes that is to say list of list of genes.


        >>> random.seed(1) 
	>>> createPopulation(10,3)
    	[[-1, -1, 1, -1, 1, 1, 1, 1, -1, -1], [1, -1, 1, 1, -1, 1, 1, -1, -1, 1], [-1, -1, -1, -1, 1, -1, 1, -1, -1, 1]]
    """
    population=[]
    for i in range(totalNumPop):
      population.append(createGenome(sizeT))
    return population

def scores(population,r):
    """ Calculate score, that is to say the cost of each genome using "evaluate" function. 
    
        Parameters
        ----------
        population : list
            list of genomes
        r : int
            The range [-r,r] = threshold.
            33
        
        Returns
        -------
        list
            list of scores for each genome. 


        >>> scores([[1, -1, -1, -1, -1], [1, -1, 1, -1, -1]], 1)
        [2, 0]

        >>> scores([[1, 1, -1, 1, -1, -1, 1, 1, -1, 1], [-1, -1, 1, -1, 1, 1, -1, 1, 1, 1]],2)
        [0, 0]
    """
    scoresTab=[]
    for ind in population:
        scoresTab.append(evaluate(ind,r))
    return scoresTab

def listAsSort(population,r):
    """Orders a population
      
      Parameters
      ----------
      population : list of genomes
          A list of genomes.
                  
      r : int
          Threshold
          

      Returns
      -------
      sortedscores : list of ints
          Returns sortedscores


      >>> listAsSort([[-1, -1, 1], [1, -1, 1], [-1, -1, -1]],1)
      array([1, 0, 2])
      """
    scores=[]
    for ind in population:
      scores.append(evaluate(ind,r))
    return np.argsort(scores)
  
def sortPopulation(population,r):
  """Sorts a population. The first genome of the list has the lower score. 
      
     Parameters
     ----------
     population : list of genomes
          A list of genomes.
                  
     r : int
          Threshold
       
     Returns
     -------
     newlist : list of genomes
          Returns the population sorted.


     >>> sortPopulation([[1,1,1,1],[1,-1,1,-1], [1,1,1,-1]],2)
     [[1, -1, 1, -1], [1, 1, 1, -1], [1, 1, 1, 1]]
  """
  sort=listAsSort(population,r)
  newlist = [[] for i in range(len(population))]
  for (i,j) in enumerate(sort):
    newlist[i]=population[j]  
  return newlist
  
def selectPopulation(population,r,nbrOfEllements):
  """Selects the best genomes.
  
      Parameters
      ----------
      population : list of genomes
          A list of genomes.
                  
      r : int
          Threshold
      
      nbrOfEllements : int
          The number of genomes to be chosen.
       
      Returns
      -------
      newlist : list of genomes
          Returns the population sorted.

      
      >>> random.seed(1)
      >>> selectPopulation([[-1, -1, 1], [1, -1, 1], [-1, -1, -1]],1,2)
      [[-1, -1, 1], [-1, -1, -1]]
  
  """
  nbr=len(population)-nbrOfEllements-1
  tab=[]
  sort=listAsSort(population,r)
  for (i,j) in enumerate(sort):
    if i > nbr:
      tab.append(population[j])
  return tab
  
def muteGene(genome,mutationRate):
  """Changes randomly a certain percentage of the genome. The genome you insert as a variable is modified.
  
      Parameters
      ----------
      genome : list of ints
          A list of ints.
                  
      mutationRate : int
          Percentage of numbers that must be mutated in the genome.
      
      Returns
      -------
      None


      >>> genome=[-1, -1, 1]
      >>> muteGene(genome,1)
      >>> genome
      [1, 1, -1]

      >>> genome=[-1, -1, 1]
      >>> muteGene(genome,0)
      >>> genome
      [-1, -1, 1]
      
      >>> random.seed(1)
      >>> genome=[-1, -1, 1]
      >>> muteGene(genome,0.5)
      >>> genome
      [1, -1, 1]

  """
  for i in range(len(genome)):
    if random.random()<mutationRate:
      genome[i]=-genome[i]

def mutePop(population, mutationRate):
  """Mutates every genome in population. The population you insert as a variable is modified.
  
      Parameters
      ----------
      population : list of genomes
          The list of genomes you want modified.
                  
      mutationRate : int
          Percentage of numbers that must be mutated in the genome.
      
      Returns
      -------
      None


      >>> pop=[[-1, -1, 1], [-1, 1, 1], [1, 1, -1]]
      >>> mutePop(pop, 1)
      >>> pop
      [[1, 1, -1], [1, -1, -1], [-1, -1, 1]]

      >>> pop=[[-1, -1, 1], [-1, 1, 1], [1, 1, -1]]
      >>> mutePop(pop, 0)
      >>> pop
      [[-1, -1, 1], [-1, 1, 1], [1, 1, -1]]

      >>> random.seed(1)
      >>> pop=[[-1, -1, 1], [-1, 1, 1], [1, 1, -1]]
      >>> mutePop(pop, 0.5)
      >>> pop
      [[1, -1, 1], [1, -1, -1], [1, 1, 1]]
   """
  for i in population:
    muteGene(i,mutationRate)
      
def crossing(population, crossRate):
  """Crosses over the genomes of your population. Selects a certain number of genomes that are crossed over with other genomes. Crossing over means that the begining of one genome is echanged with the begining of an other genome.

      Parameters
      ----------
      population : list of genomes
          The list of genomes you want modified.

      crossRate : int
          Percentage of genomes that must be crossed over in the population.

      Returns
      -------
      None


      >>> population=[[-1, -1, 1], [1, -1, 1], [-1, -1, -1]]
      >>> crossing(population,0)
      >>> population
      [[-1, -1, 1], [1, -1, 1], [-1, -1, -1]]

      >>> random.seed(1)
      >>> population=[[-1, -1, 1], [1, -1, 1], [-1, -1, -1]]
      >>> crossing(population,1)
      >>> population
      [[-1, -1, -1], [1, -1, 1], [-1, -1, 1]]
  """
  for genome in population:
    if random.random()<crossRate:
      othergene=random.choice(population)
      cutIndex=random.randint(0,len(othergene)-1)
      tmp=genome[cutIndex:]
      genome[cutIndex:]=othergene[cutIndex:]
      othergene[cutIndex:]=tmp

def selectAndMute(population,r,nbrOfEllements,crossRate,muteRate):
  """Selects the worst genomes and mutates them. Mutating a population consists of mutating the genomes and the crossing them over.

      Parameters
      ----------
      population : list of genomes
          The list of genomes you want modified.

      r : int
          Treshold after which each step in the random walk costs. 

      nbrOfEllements : int
          The number of genomes that are selected to be modified. The genomes that are selected are the worst ones oh the population.

      crossRate : float
          Percentage of genomes that must be crossed over in the worst genomes.

      muteRate : float
          Percentage of genes that are modified in the worst genomes.
                  
      Returns
      -------
      None


      >>> random.seed(1)
      >>> pop = [[-1, -1, 1, -1], [1, 1, 1, 1], [-1, -1, 1, -1], [1, 1, -1, 1]]
      >>> selectAndMute(pop, 1, 2, 0.5, 0.5)
      >>> pop
      [[-1, -1, 1, -1], [-1, -1, 1, -1], [-1, -1, 1, -1], [-1, 1, -1, 1]]
  """

  tab=[]
  sort=listAsSort(population,r)
  tab=sort[len(population)-nbrOfEllements:len(population)]
  c = [population[index] for index in tab] 
  mutePop(c,muteRate)
  crossing(c,crossRate)


def mutePopulation(population,r,choose,Tc,Tm, iteration,average):
    """Mutates the entire population over multiple generations. The average score of a population and the score of the best genome of each generation is returned. A graph is also ploted.

      Parameters
      ----------
      population : list of genomes
          The list of genomes you want modified.

      r : int
          Treshold after which each step in the random walk costs. 

      choose : float
          The percatage of genomes that are selected to be muted.

      Tc : float
          CrossRate : Percentage of genomes that must be crossed over in the worst genomes.
          
      Tm : float
         MuteRate : Percentage of genes that are modified in the worst genomes.

      iterartion : int
         The number of iteration that will be excecuted. One iteration is equal to a generation
          
      average : bool
         True if you want the graph to represent the average of each generation or the best of each generation
          
      Returns
      -------
      (scorepergeneration,bestpergeneration) : tuple with the two lists. First list contains the average score of a population and the second list the score of the best genome.


      >>> random.seed(1)
      >>> pop = [[-1, -1, 1, -1], [1, 1, 1, 1], [-1, -1, 1, -1], [1, 1, -1, 1]]
      >>> mutePopulation(pop,r=0.5,choose=0.5,Tc=.5,Tm=.5, iteration=10,average=True)
      ([3.75, 3.0, 3.0, 2.5, 2.75, 2.75, 2.5, 2.25, 2.75, 2.0], [3, 2, 2, 2, 2, 2, 2, 2, 2, 2])
      """
    sizeT=len(population[0])
    sizePopulationN=len(population)
    choose=int(choose*len(population))
    scorepergeneration=[]
    bestpergeneration=[]
    for i in range(iteration):
      selectAndMute(population,r,choose,Tc,Tm) 
      sc=scores(population,r)
      scorepergeneration.append(sum(sc) / len(sc))
      bestpergeneration.append(min(sc))
    if average :
        plt.plot(scorepergeneration,label=str("Average of Generation "+" r "+str(r)+" Nbr of elements chosen "+str(choose)+" Tc "+str(Tc)+" Tm "+str(Tm)+ " iterations "+str(iteration)+" size of genome "+str(sizeT)+" size of population "+str(sizePopulationN)))
    else:
        plt.plot(bestpergeneration,label=str("Best of Generation "+" r "+str(r)+" Nbr of elements chosen "+str(choose)+" Tc "+str(Tc)+" Tm "+str(Tm)+ " iterations "+str(iteration)+" size of genome "+str(sizeT)+" size of population "+str(sizePopulationN)))
  
    return (scorepergeneration,bestpergeneration)

if __name__ == "__main__":
    import doctest
    doctest.testmod()


