Introduction
============

.. figure::  _static/intro.jpg
   :align:   center


To download module :

.. code-block:: bash

   pip install -i https://test.pypi.org/simple/ Carlier-Loppinet==0.0.3


or download it from `gitlab <https://gitlab.com/tomlop/devlog>`_.

This project was created by Maxime and Thomas during the AI class of the 4th year of bioinformatics of INSA Lyon.
Have lots of fun using this awesome package. If you need help use the documentation page or the exemples.

For exemples on how to use this module check out :
 * :download:`notebook <_static/Bioinspired_Algorithm_example.ipynb>`
 * :download:`python scipt <_static/Bioinspired_Algorithm_example.py>`


Report
------
This is a module that will help you create genomes and mutate them. It can be helpful for AI and optimisation in random walk problems. A whole report can be found on this  :download:`pdf <_static/Bioinspired_Algorithm.pdf>` document.


