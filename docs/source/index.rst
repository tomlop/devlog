.. DevLog documentation master file, created by
   sphinx-quickstart on Wed Apr 15 13:40:51 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.



Welcome to deployment project documentation!
============================================

.. toctree::
   :maxdepth: 2

   intro
   modules


Indices and tables
==================


* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
