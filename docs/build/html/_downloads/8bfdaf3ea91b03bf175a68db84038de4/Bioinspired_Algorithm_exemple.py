#!/usr/bin/env python
# coding: utf-8

# # Package_Bioinspired_Algorithm

# In[28]:


import Package_Bioinspired_Algorithm as bia
import matplotlib.pyplot as plt
import copy


# ## Choose the parameters of the population of genomes

T= 500 #taille de genome
N=100 #nbr d individu


# ## Create the population

population_original=bia.createPopulation(T,N)


r=4 # threshold
Tm= 0.05 #Taux de mutation
Tc= 0.1 #Taux de recombinaison
choose=0.5 #Percantage of genomes that will be mutated
number_of_generations=10


bia.mutePopulation(population_original,r,choose,Tc,Tm, number_of_generations, True)


## More comlicated exemple

### Exemple of search for best value of "Percantage of genomes that will be mutated"

#### Graphe with the average of each generation
for choose in [.20,.25,.30,.35,0.4]:
    population = copy.deepcopy(population_original)
    bia.mutePopulation(population,r,choose,Tc,Tm, 500, True)
plt.title("Averege of genome by generation")   
plt.xlabel("generation")
plt.ylabel("score")
plt.legend(bbox_to_anchor=(1, 0), loc="best",fontsize ='small', ncol=1)
plt.show()


#Graphe with minimum of each generation
for choose in [.20,.25,.30,.35,0.4]:
    population = copy.deepcopy(population_original)
    bia.mutePopulation(population,r,choose,Tc,Tm, 500, False)
plt.title("Best of genome by generation")   
plt.xlabel("generation")
plt.ylabel("score")
plt.legend(bbox_to_anchor=(1, 0), loc="best",fontsize ='small', ncol=1)
plt.show()

